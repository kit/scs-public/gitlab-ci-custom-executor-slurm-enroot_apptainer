SimpleCov.start do
  add_filter "/tests/"
  add_filter "/.git/"
end
