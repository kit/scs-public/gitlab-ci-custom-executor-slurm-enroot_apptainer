#!/usr/bin/env bash

setup(){
    load '../test_helper/bats-support/load' # for run
    load '../test_helper/bats-assert/load' # for assert_output and refute_output
    load '../test_helper/bats-file/load' # for assert_output and refute_output
    load '../test-utils'

    PROJECT_ROOT="$(cd "$(dirname "$BATS_TEST_FILENAME" )/../.." > /dev/null 2>&1 && pwd)"
    export PROJECT_ROOT

    load_mocks
    TEST_PLAYGROUND="$PWD/enroot-mock-test-$BATS_TEST_NAME"
    echo "# Creating test playground at $TEST_PLAYGROUND" >&3
    mkdir "$TEST_PLAYGROUND"
    STARTDIR="${TEST_PLAYGROUND}"/start
    mkdir -p "$STARTDIR"
    cd "$STARTDIR" || exit
    echo "Created test playground at $TEST_PLAYGROUND"
    export MOCKLOGDIR=$TEST_PLAYGROUND

    export ENROOT_MOCK_PATH=/some/path
}

teardown() {

    try_to_remove_dir "$TEST_PLAYGROUND"

}
@test "enroot mock complains if no log location is given" {

    (
    # shellcheck disable=SC2030
    export MOCKLOGDIR=
    run enroot --something
    assert_equal "$status" 45
    )

}


@test "enroot mock record logs at given location" {

    enroot --something
    # shellcheck disable=SC2031
    assert_file_exists "$MOCKLOGDIR"/enroot.log

}

@test "enroot mock records ENROOT* environment variables" {


    export ENROOT_SOME_PATH=/test/path

    enroot --something

    # shellcheck disable=SC2031
    assert_file_contains "$MOCKLOGDIR"/enroot.log "ENROOT_SOME_PATH"

}

@test "enroot mock records its stdin when using remove" {

    # shellcheck disable=SC2031
    FAKECONTAINER="$MOCKLOGDIR/.enroot-fakecontainers/thisthing"
    mkdir -p "$(dirname "$FAKECONTAINER")"
    touch "$FAKECONTAINER"

    enroot remove thisthing < <(echo "some kind of input")

    # shellcheck disable=SC2031
    assert_file_contains "$MOCKLOGDIR"/enroot.log "some kind of input"


}

@test "enroot mock creates file named as container and lists it" {

    enroot create --name thiscontainer imagename-ignored.sqsh
    enroot create --name thatcontainer imagename-ignored.sqsh

    run enroot list

    assert_output --partial thiscontainer
    assert_output --partial thatcontainer

}

@test "enroot remove removes containers " {

    enroot create --name thiscontainer imagename-ignored.sqsh
    enroot create --name thatcontainer imagename-ignored.sqsh
    enroot remove thiscontainer < <(echo yes)
    run enroot list
    refute_output --partial thiscontainer

}

@test "enroot start runs the script after -- and after container name" {

    cat <<EOF > ./script.sh
echo "something"
EOF
    chmod +x ./script.sh

    run enroot start -- container-name ./script.sh
    assert_output "something"

}

@test "enroot create needs name and image filename" {

    run enroot create --name thiscontainer

    assert_failure
    assert_output --partial "Image name missing!"

}

@test "enroot import creates sqsh file when called with output" {

    enroot import --output thisfile.sqsh docker://thisimage:latest

    assert_exists thisfile.sqsh


}
