# Feature backlog and discussion

This file contains a list of features 
and points to the tests 
where each of the features is tested.

Note: we might need to run experiments 
on the systems we are interacting with
(both the gitlab stack and the hpc cluster)
to clarify some aspects of that.

THIS FILE MIGHT NOT BE UP TO DATE.
IT IS JUST A SPACE TO STORE IDEAS 
AND KEEP TRACK OF THE STATUS OF THEIR IMPLEMENTATION.
THE REAL STATUS OF WHAT IS IMPLEMENTED AND WHAT NOT
IS IN THE TESTS.
THIS FILE IS MAINTAINED ON A BEST-EFFORT BASIS.


## Functional

### Custom executor queue

- [X] custom executor queue should die gracefully when killed with SIGTERM and print a message
      (It takes a while to die)
- [X] custom executor queue should die gracefully when killed with SIGUSR1 and print a message
      (It takes a while to die)
- [X] custom executor queue should die and print a message even if a job is running
- [X] custom executor queue should pick up jobs and execute them
- [X] submitting jobs to the custom executor queue 
- [X] the custom executor queue should give back stdin and stdout
      (in the same file for simplicity)
- [X] the exit code of the jobs should be stored somewhere accessible
- [ ] when a script is submitted, 
      it should be checked that the custom executor queue is running.
      This might require checking the status of the process of the custom executor queue
      (the relative pid must be stored in the right place)
      and/or that the slurm job where the custom executor queue is running
      is still up.

      

Additional features that should make it work more or less like slurm

- [X] a script submitted to the system should be executed 
      in the same directory as it was submitted
- [X] when a script is submitted,
      the environment at the submission should be used at runtime,
      and will be merged on the environment in which the custom executor queue is running.
- [X] not all variables from the submission environment are used,
      based on a blacklist 
      (we want some variables not to be changed 
      when running in the custom executor queue)
     


### configure.sh
Preparation of the slurm allocation was done here,
and the information on how to reach it 
were saved in a json that contains an environment section,
so that in subsequent steps it is available through environment variables.

Here we need to create the slurm allocation
with the right parameters
start the "custom executor queue" here, 
make the "custom executor queue" reachable.

Considerations:

- variables (which might be used to select slurm resources)
  are presented to this script by the runner prepended with
  `CUSTOM_ENV`.
  The right ones (starting with SLURM)
  need to be renamed so that sbatch can pick them up.
- unfortunately `sbatch` cannot take all the job parameters
  as environment variables.
  For this reason, we need to provide a way to pass parameters
  to the sbatch invocation.

Features:


  NOTE: this might need adjustments as we go along.

- [ ] if CUSTOM_ENV_CI_WS is not defined, clear error message and hint should be printed
  - [X] Unit
  - [ ] Integration
- [X] allocation manager is started in slurm allocation 
- [X] pass input variables on to sbatch
- [X] pass content of variable `COMMAND_OPTIONS_SBATCH` to `sbatch` invocation
- [X] when `COMMAND_OPTIONS_SBATCH` is not defined, do lament on stderr (but continue)
- [X] use reasonable defaults for `sbatch` invocation 
      when `COMMAND_OPTIONS_SBATCH`not set 
- [ ] `configure` should output valid json *in all cases* 
      (this is an ongoing work item)
- [ ] custom executor queue uses `$CUSTOM_ENV_CI_WS/cews_N` as workspace
      where N>=0 is the small possible number 
- [X] `job_env` should contain the information needed to next steps
  - [X] location of the workspace given to `start_custom_executor_queue` 
- [ ] `sbatch` should be invoked 
      with all the necessary additional options.
      (See Holger's code).
      NOTE: to test this with a mock, one would have to implement 
      the corresponding features of the real `sbatch`, 
      which is very likely a bad use of our time.
      We can have two kinds of tests:
      - The unit test that just verify that the options are passed to the sbatch mock
      - The integration test that verifies the effect on the slurm allocation
  - Only SLURM_* variables from the user environment will be defined: `--export=NONE`
    This might be tested with an integration test that checks 
    no variable is exported to the to the custom executor queue 
    (we need to check instead that variables are exported and how
    if we do not use that `--export=NONE` flag)
    ISSUE: This does not work with pyxis! DISCUSS
    - [ ] Unit
    - [ ] Integration
  - None of the modifiable (soft) resource limits are propagated `--propagate=NONE`
    (how to test that at the integration level?)
    - [X] Unit
    - [ ] Integration
  - Set / as the working directory of the batch script `--chdir=/`
    Note: is this what we actually want here? The setup is more complicate than in Holger's case.
    This might be trivially tested with an integration test
    DISCUSS
    - [ ] Unit
    - [ ] Integration
  - Connect the batch script's standard input, output and error  with `/dev/null`: `--input=/dev/null` `--output=/dev/null` `--error=/dev/null`
    This does not happen when the variable `CUSTOM_ENV_CE_DEBUG_LVL` is set to 2 or more.
    - [X] Unit
    - [ ] Integration

- [X] SIGUSR1 should be sent to the job when the allocation is about to run out of time
  (and the custom executor queue should terminate)
  - [X] Unit
  - [X] Contract 

### containerlib-enroot.sh
 
 - [X] should define the necessary `ENROOT_*_PATH` variables
  so that they can be used in `prepare.sh` but also in `run.sh` and `cleanup.sh`,
  starting from standard gitlab environment variables
  - [X] ONLY IF `CUSTOM_ENV_CI_JOB_IMAGE` is set
 - [X] should define the variable `CE_CONTAINER_NAME` 
  starting from standard gitlab environment variables
  - [X] ONLY IF `CUSTOM_ENV_CI_JOB_IMAGE` is set
 

### prepare.sh
prepare should actually run in the allocation, I believe.

#### enroot
Preparation of the image/container is done here 
in Holger's example.
It will source `containerlib.sh`.

- [ ] download image (using import)
  - [X] Unit
  - [ ] Integration
- [ ] remove existing container with the same name 
  - [X] Unit
  - [ ] Integration
- [X] create container with the container name 
  defined in the configure stage (with create)
- [X] Uses paths inside `CUSTOM_ENV_CI_WS`
      (except for `ENROOT_TEMP_PATH`)
- [X] do not do anything if image is not defined

#### Apptainer
Enroot and apptainer work in quite a different way.
In the way that it is configured on HoreKa, 
apptainer will by default mount PWD and HOME.
This means that 
- [ ] download image (using pull)
  - [X] Unit
  - [ ] Integration
- [ ] Uses paths inside `CUSTOM_ENV_CI_WS`
  At the moment it is only about `APPTAINER_CACHEDIR`.
  - [X] Unit
  - [ ] Integration

### run.sh

Every step runs in the allocation, 
the point is whether or not it needs to be 
wrapped into a `enroot`/`apptainer` command.

- `prepare_script`
   This might need to run in the allocation.
- `get_sources`
   it needs to run outside the container,
   since `git` might not be available inside,
   but needs to be able to get the code in a place visible to the allocation.
   It also needs access to network.
- `restore_cache`
   it needs to be able to run outside the container,
   since it needs to see the `gitlab_runner` executable
- `download_artifacts`
   it needs to be able to run outside the container,
   since it needs to see the `gitlab_runner` executable
- `step_*`
   A group of files, that needs to be run inside the allocation 
   and the container
- `build_script`
   This needs to run in the container in the allocation
- `step_*`
   A group of files, that needs to be run inside the allocation 
   and the container (yes, this is repeated after `build_scritpt`)
- `after_script`
   This is run inside the container now (in the enroot executor),
   so this will go inside the allocation
- `archive_cache` OR `archive_cache_on_failure`
   I suspect this needs to run outside the container,
   since so far cache operations need to see the `gitlab_runner` executable
   (see `restore_cache`)
- `upload_artifacts_on_success` OR `upload_artifacts_on_failure`
   I suspect this needs to run outside the container,
   since so far artifact operations need to see the `gitlab_runner` executable
- `cleanup_file_variables`
   At the moment this is done in the container.
   THIS NEEDS TO BE TESTED

- [ ] The exported variables might need to be passed to the submitted file
      DISCUSS
- [ ] Every step which is submitted to the custom executor queue
      needs to have its output redirected from the file where it is printed
      to the stdout of this process (use `tail -f ...`? Or just "cat" at the end)
   - [X] Unit
   - [ ] Integration
- [ ] `run` must exit with the same error as the submitted job
   - [X] Unit
   - [ ] Integration
- wrap the script in an `enroot` command, if
  - [ ] An image is specified
    - [X] Unit
    - [ ] Integration
  - [ ] the step name is among those that need to be run in a container.
    - [X] Unit
    - [ ] Integration
  
  The wrap can then be submitted to the custom executor queue 
  (see the "command wrapper" section)

#### enroot command wrapper
- [X] set up the enroot environment variables 
- [X] invoke enroot with the right mount points
- [X] use `--rw`
- [X] use the container name defined in the prepare stage
  (define an environment variable for that in configure-produce job-env?)

#### apptainer command wrapper
on HoreKa by default PWD is bound on the container 
with RW permissions,
so there should be no need 
for `--writable` (or similar) flag.

- [ ] uses `apptainer exec`

### cleanup.sh
- [ ] The slurm job needs to be terminated if `CUSTOM_ENV_KILL_SLURM` is set 
  - [ ] `--signal=SIGUSR1` should be used first
    - [X] Unit
    - [ ] Integration
  - [ ] standard `scancel` should be used after 5 seconds
    - [X] Unit
    - [ ] Integration
  - [ ] Optional: use '-f' as well
    To send the signal to every child 
    (this should not be necessary
    given how configure and `job.slurm` work).
    - [X] Unit
    - [ ] Integration

- [X] old custom executors workspaces need to be deleted
- [X] if `CUSTOM_ENV_CE_DEBUG_LVL` is set, 
      the directories are copied into "$HOME/ce-debug"  for later inspection.
- [ ] containers created in the prepare.sh step need to be removed
  - [X] enroot
        Note: this is already done by deletion 
        of the temporary directories IF THE NECESSARY SUBSET
        of `ENROOT_*_PATH` variables are set 
        to point inside the right directories.
  - [X] apptainer
    - removing CE_IMAGEFILE
  - [X] unless `CUSTOM_ENV_CE_DEBUG_LVL` is set

   
## Diagnostics
- should check the existence of the programs used
- should check the right environment variables are set up 
- when a step fails, it should give information about that

## Considerations
- It is not clear to me what is the difference 
  between using `sbatch --container` as an option
  or `sbatch --container-image`, 
  or creating the image manually and then loading it into enroot
  (but then this might not work when running on multiple nodes) 
- when running on multiple nodes
  in a container,
  it is not clear how to have a portable method
  (e.g., one that is independent of the MPI implementation used).
  I have found no precise information for enroot,
  but this is what one should do with Singularity and OpenMPI:
  https://docs.sylabs.io/guides/3.3/user-guide/mpi.html

  OpenMPI processes needs to talk to the ORTE Daemon
  that needs to be started automatically.
  
  In the case of Enroot, 
  apparently an environment variable
  can be used to tell the OpenMPI runtime
  how to start orted:
  https://github.com/NVIDIA/enroot/issues/49
  This unfortunately does not seem to work 
  on HoReKa (for multiple nodes).
  
  
  
- preparation of the allocation and of the image
  is done in two different steps at the moment
  (in the two slurm/enroot driver)
  This might actually need to change now.

## Ideas
- test bootstrapping: testing the custom executor 
  on the custom executor itself?


